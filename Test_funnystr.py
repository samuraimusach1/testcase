import unittest
from Xfunnystring import funnyString

class TestFunnyString(unittest.TestCase):
    def test_give_abc_is_funny(self):
        s = "abc"
        is_funny = funnyString(s)
        self.assertEqual(is_funny, "Funny")
    
    def test_give_bcd_is_not_funny(self):
        s = "bcd"
        is_not_funny = funnyString(s)
        self.assertEqual(is_not_funny, "Not Funny")
    
    def test_give_bcde_is_funny(self):
        s = "bcde"
        is_funny = funnyString(s)
        self.assertEqual(is_funny, "Funny")

if __name__ == '__main__':
    unittest.main()
