
import math
import os
import random
import re
import sys

def caesarCipher(s, k):
    result = []
    for c in s:
        if c.islower():
            new_char = chr((ord(c) - 97 + k) % 26 + 97)
        elif c.isupper():
            new_char = chr((ord(c) - 65 + k) % 26 + 65)
        else:
            new_char = c
        result.append(new_char)
    return ''.join(result)


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input().strip())

    s = input()

    k = int(input().strip())

    result = caesarCipher(s, k)

    fptr.write(result + '\n')

    fptr.close()
