import math
import os
import random
import re
import sys

#
# Complete the 'alternate' function below.
#
# The function is expected to return an INTEGER.
# The function accepts STRING s as parameter.
#

def alternate(s):
    distinct_chars = list(set(s))
    n = len(s)
    longest_valid_string = 0
    for i in range(len(distinct_chars)):
        for j in range(i+1, len(distinct_chars)):
            prev_char = None
            valid = True
            count = 0
            for k in range(n):
                if s[k] == distinct_chars[i] or s[k] == distinct_chars[j]:
                    if prev_char == s[k]:
                        valid = False
                        break
                    prev_char = s[k]
                    count += 1
            if valid and count > longest_valid_string:
                longest_valid_string = count
    return longest_valid_string if longest_valid_string > 1 else 0


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    l = int(input().strip())

    s = input()

    result = alternate(s)

    fptr.write(str(result) + '\n')

    fptr.close()
