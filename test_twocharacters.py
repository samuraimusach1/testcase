import unittest
from Xtwocharacters import alternate

class TestAlternate(unittest.TestCase):
    
    def test_only_one_character(self):
        s = "a"
        self.assertEqual(alternate(s), 0)
        
    def test_all_same_characters(self):
        s = "bbbbbb"
        self.assertEqual(alternate(s), 0)
        
    def test_only_two_different_characters(self):
        s = "ababab"
        self.assertEqual(alternate(s), 6)
        
    def test_all_different_characters(self):
        s = "abcde"
        self.assertEqual(alternate(s), 2)
        
if __name__ == '__main__':
    unittest.main()
