import unittest
from Xalternating import alternatingCharacters

class TestAlternatingCharacters(unittest.TestCase):
    def test_alternatingCharacters(self):
        self.assertEqual(alternatingCharacters('one'), 0)
        self.assertEqual(alternatingCharacters('two'), 0)
        self.assertEqual(alternatingCharacters('three'), 1)
        self.assertEqual(alternatingCharacters('four'), 0)
        self.assertEqual(alternatingCharacters('five'), 0)

    def test_empty_string(self):
        self.assertEqual(alternatingCharacters('a'), 0)

    def test_single_character_string(self):
        self.assertEqual(alternatingCharacters('b'), 0)

    def test_string_with_two_characters(self):
        self.assertEqual(alternatingCharacters('six'), 0)
        self.assertEqual(alternatingCharacters('seven'), 0)
        self.assertEqual(alternatingCharacters('eight'), 0)

    def test_string_with_three_characters(self):
        self.assertEqual(alternatingCharacters('nine'), 0)
        self.assertEqual(alternatingCharacters('ten'), 0)
        self.assertEqual(alternatingCharacters('eleven'), 0)
        self.assertEqual(alternatingCharacters('twelve'), 0)

if __name__ == '__main__':
    unittest.main()
