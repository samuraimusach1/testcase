import unittest
from Xcaesarcipher import caesarCipher

class TestCaesarCipher(unittest.TestCase):
    def test_empty_string(self):
        self.assertEqual(caesarCipher("", 3), "")
        
    def test_no_rotation(self):
        self.assertEqual(caesarCipher("hello", 0), "hello")
        
    def test_all_caps(self):
        self.assertEqual(caesarCipher("HELLO WORLD", 5), "IJKLM NOPQR")
        
    def test_mixed_case(self):
        self.assertEqual(caesarCipher("The Quick Brown Fox Jumps Over 13 Lazy Dogs", 7), "ABC DEFGH IJKLM NOP QRSTU VWXY Z1 2345 6789")
        
    def test_special_characters(self):
        self.assertEqual(caesarCipher("1-2-3 GO!", 4), "1-2-3 KCS!")
        
if __name__ == '__main__':
    unittest.main()
