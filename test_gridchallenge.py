import unittest
from Xgridchallenge import gridChallenge

class TestGridChallenge(unittest.TestCase):
    def test_gridChallenge(self):
        # Test case 1
        input1 = ["abc", "def", "geh"]
        output1 = "YES"
        self.assertEqual(gridChallenge(input1), output1)
        
        # Test case 2
        input2 = ["cham", "jaka", "sota"]
        output2 = "NO"
        self.assertEqual(gridChallenge(input2), output2)

if __name__ == '__main__':
    unittest.main()
